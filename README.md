Word Finder
======

This small project was inspired by the app ['4 pics one word'](https://itunes.apple.com/au/app/4-pics-1-word/id595558452?mt=8). While brushing up on my Python skills, I decided to try and make a word finder that operates:

1. The length of the word
2. The set of letters

Speed optimisation can come later. (Though it's somewhat build in now with the file indexing).

## Usage (Python 3.5)
Run the `word_file_sorter.py` file. This will then make files of all the different word lengths. Then run the `word_finder.py` file and enter your desired parameters when prompted. It will then print all the valid words with that length and letter subset within the english dictionary. Two functions were made to compare performance. As it turns out, the file split version can be orders of magnitude faster.


### Dictionary
https://github.com/dwyl/english-words

