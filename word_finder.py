'''
Word Finder (Naive)

Josip Simunic 2016

Given a word length and a set of letters, it will find potential words. This is the naive version, for performance comparison purposes. The following code is the naive dictionary search. This simply goes through the whole dictionary and compares the word to the word set. Depending on the word length, the speed difference can be orders of magnitude faster.
'''
import time
import os.path


# This is the faster version, using the files to quickly skip the to the correct lengths.
def word_finder(word_length, letter_set, printout):
	valid_words = []
	# If the file exists, we can use it.
	if os.path.isfile(str(word_length) + '_letter_words.txt'):
		words = open(str(word_length) + '_letter_words.txt', 'r')
	else:
		return 0
	for line in words:
		# If the word is a subset of the letter set, it's a valid option
		if set(line.strip()).issubset(letter_set):
			valid_words.append(line.strip())
	if printout:
		print ('Valid words:', valid_words)
	words.close()
	return len(valid_words)


# This is the slower version, having to go through the whole dictionary.
def word_finder_naive(word_length, letter_set, printout):
	valid_words = []
	words = open('dictionary.txt', 'r')
	for line in words:
		if (len(line) - 1 == word_length) & (set(line.strip()).issubset(letter_set)):
			valid_words.append(line.strip())
	if printout:
		print ('Valid words:', valid_words)
	words.close()
	return len(valid_words)
	

# Timing the execution of the methods.
def word_finder_test():
	for x in xrange(2,33):

		start1 = time.time()
		a = word_finder(x, 'joeiscoolawesomesauce', False)
		end1 = time.time()

		start2 = time.time()
		b = word_finder_naive(x, 'joeiscoolawesomesauce', False)
		end2 = time.time()

		print('length:%d\tfast:%f\tnaive:%f words found:\t%d,%d' % (x, end1-start1, end2-start2, a, b))


# Calling the test
# word_finder_test()

# Running the script for inputs
length = int(input('How long is your word? '))
letters = input('What are the letters in your word? ')
word_finder(length, letters, True)

