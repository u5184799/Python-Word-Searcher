# Word Finder
# Josip Simunic 2016

# This document is used to split the words into different files for faster indexing.


# Want to read the dictionary first to see the longest word.
dict_lines = open('dictionary.txt', 'r')
max_word_length = 1;
for line in dict_lines: 
	max_word_length = max(len(line), max_word_length)

# Now split all the words into different files.
for length in range(2, max_word_length + 1):
	# Read in the dictionary.
	dict_lines = open('dictionary.txt', 'r')
	# Create a new file
	newfile = open(str(length) + '_letter_words.txt', 'w')
	# If it's the length we want, add it to the file.
	for line in dict_lines:
		# Ignore newline char.
		if len(line) - 1 == length:
			newfile.write(line)
	newfile.close()
	dict_lines.close()